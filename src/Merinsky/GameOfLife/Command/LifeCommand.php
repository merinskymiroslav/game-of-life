<?php

namespace Merinsky\GameOfLife\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * LifeCommand
 *
 * @author Miroslav Merinsky <miroslav@merinsky.biz>
 * @version 1.0
 * @licence GPL version 3 http://www.gnu.org/licenses/gpl-3.0.html
 */
class LifeCommand extends Command
{

    /**
     */
    protected function configure()
    {
        $this->setName('life:life')
             ->setDescription('Launches interactions on input file representation of life.')
             ->addArgument('input', InputArgument::REQUIRED, 'The input file which contains XML representation of a life.')
             ->addArgument('output', InputArgument::REQUIRED, 'The output file where life representation will be stored after interactions.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $life = new \Merinsky\GameOfLife\LifeExtended();
        $life->fromXML($input->getArgument('input'));

        for ($i = 1; $i <= $life->getInteractions(); $i++) {
            \Merinsky\GameOfLife\LifeRules::applyDie($life);
            \Merinsky\GameOfLife\LifeRules::applyOvercrowding($life);
            \Merinsky\GameOfLife\LifeRules::applyBirth($life);
        }
        $life->setInteractions(0);

        $life->toXML($input->getArgument('output'));
    }

}
