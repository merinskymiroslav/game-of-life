<?php

declare(strict_types = 1);

namespace Merinsky\GameOfLife;

/**
 * Life
 *
 * @author Miroslav Merinsky <miroslav@merinsky.biz>
 * @version 1.0
 * @licence GPL version 3 http://www.gnu.org/licenses/gpl-3.0.html
 */
class Life
{
    // represents empty cell
    const EMPTY_CELL = '';

    /** @var int Dimension of the square "world" */
    protected $dimension;

    /** @var int Number of distinct species */
    protected $species;

    /** @var int Number of iterations to be calculated */
    protected $interactions;

    /** @var array Square $dimension x $dimension, represents species world */
    protected $organisms = [];


    /**
     * @param int $dimension
     */
    public function setDimension(int $dimension)
    {
        if ($dimension <= 0)
            throw new \InvalidArgumentException('Invalid argument has been entered.');

        $this->dimension = $dimension;
        $this->initWorld();
    }

    /**
     * @param int $species
     */
    public function setSpecies(int $species)
    {
        if ($species <= 0)
            throw new \InvalidArgumentException('Invalid argument has been entered.');

        $this->species = $species;
    }

    /**
     * @param int $interactions
     */
    public function setInteractions(int $interactions)
    {
        if ($interactions < 0)
            throw new \InvalidArgumentException('Invalid argument has been entered.');

        $this->interactions = $interactions;
    }

    /**
     * @param int $x
     * @param int $y
     * @param string $type
     */
    public function setOrganism(int $x, int $y, string $type)
    {
        if (($x <= 0) || ($x > $this->dimension)
            || ($y <= 0) || ($y > $this->dimension)
            || empty($type))
            throw new \InvalidArgumentException("Invalid argument has been entered for $type in ${x}x$y position.");

        if (!isset($this->organisms[$x][$y]))
            throw new \UnexpectedValueException("Can not find organism cell ${x}x$y. Check, if organisms are rightly initialized.");

        $this->organisms[$x][$y] = $type;
    }

    /**
     * @param array $world
     */
    public function setWorld(array $world)
    {
        if (!isset($world[1][1]) || !isset($world[$this->dimension][$this->dimension])
            || isset($world[$this->dimension + 1][$this->dimension]) || isset($world[$this->dimension][$this->dimension + 1])
            || is_array($world[1][1]) || is_array($world[$this->dimension][$this->dimension]))
            throw new \InvalidArgumentException('Invalid argument has been entered.');

        $this->organisms = $world;
    }

    /**
     * @return int
     */
    public function getDimension(): int
    {
        return $this->dimension;
    }

    /**
     * @return int
     */
    public function getSpecies(): int
    {
        return $this->species;
    }

    /**
     * @return int
     */
    public function getInteractions(): int
    {
        return $this->interactions;
    }

    /**
     * @param int $x
     * @param int $y
     * @return string
     */
    public function getOrganism(int $x, int $y): string
    {
        if (($x <= 0) || ($x > $this->dimension)
            || ($y <= 0) || ($y > $this->dimension))
            throw new \InvalidArgumentException('Invalid argument has been entered.');

        if (!isset($this->organisms[$x][$y]))
            throw new \UnexpectedValueException("Can not find organism cell ${x}x$y. Check, if organisms are rightly initialized.");

        return $this->organisms[$x][$y];
    }

    /**
     * Returns n x n dimension square represents species world
     *
     * @return array
     */
    public function getWorld(): array
    {
        return $this->organisms;
    }

    /**
     * @return bool
     */
    public function isInitialized(): bool
    {
        return (!empty($this->organisms));
    }

    // private ---------------------------------------------------------------------------------------------------------

    /**
     */
    private function initWorld()
    {
        $this->organisms = [];

        for($i = 1; $i <= $this->dimension; $i++) {
            $this->organisms[$i] = [];

            for($j = 1; $j <= $this->dimension; $j++) {
                $this->organisms[$i][$j] = self::EMPTY_CELL;
            }
        }
    }

}
