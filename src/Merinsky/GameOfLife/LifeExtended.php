<?php

declare(strict_types = 1);

namespace Merinsky\GameOfLife;

/**
 * LifeExtended
 *
 * @author Miroslav Merinsky <miroslav@merinsky.biz>
 * @version 1.0
 * @licence GPL version 3 http://www.gnu.org/licenses/gpl-3.0.html
 */
class LifeExtended extends Life
{
    // template file which is used for validation input
    const TEMPLATE_FILE = 'life.xsd';

    /**
     * @param string $filename
     * @param string $content
     */
    public function fromXML(string $filename = null, string $content = null)
    {
        if (empty($filename) && empty($content))
            throw new \InvalidArgumentException('No argument has been entered.');
        if (isset($filename) && !is_file($filename))
            throw new \InvalidArgumentException("Can not open $filename file.");

        if (isset($filename))
            $content = file_get_contents($filename);

        // validate xml
        $xml = new \DOMDocument();
        $xml->loadXML($content);

        if (!$xml->schemaValidate(__DIR__ . '/' .self::TEMPLATE_FILE)) {
            throw new \InvalidArgumentException('Invalid XML format or structure.');
        }

        // load
        $xml = new \SimpleXMLElement($content);
        $this->setDimension((int)$xml->world->cells);
        $this->setSpecies((int)$xml->world->species);
        $this->setInteractions((int)$xml->world->iterations);

        foreach ($xml->organisms->organism as $organism) {
            $this->setOrganism((int)$organism->x_pos, (int)$organism->y_pos, (string)$organism->species);
        }
    }

    /**
     * Returns or save to file XML representations of life
     *
     * @param string $filename
     * @return string
     */
    public function toXML(string $filename = null)
    {
        $worldData = $this->getWorld();

        // create DOM
        $xml = new \DOMDocument('1.0', 'UTF-8');

        $live = $xml->appendChild($xml->createElement('live'));

        $world = $live->appendChild($xml->createElement('world'));
        $world->appendChild($xml->createElement('cell', (string)$this->getDimension()));
        $world->appendChild($xml->createElement('species', (string)$this->getSpecies()));
        $world->appendChild($xml->createElement('iterations', (string)$this->getInteractions()));

        $organisms = $live->appendChild($xml->createElement('organisms'));

        for ($x = 1; $x <= $this->getDimension(); $x++) {
            for ($y = 1; $y <= $this->getDimension(); $y++) {
                if ($worldData[$x][$y] != self::EMPTY_CELL) {
                    $organism = $organisms->appendChild($xml->createElement('organism'));
                    $organism->appendChild($xml->createElement('x_pos', (string)$x));
                    $organism->appendChild($xml->createElement('y_pos', (string)$y));
                    $organism->appendChild($xml->createElement('species', $worldData[$x][$y]));
                }
            }
        }

        if (!empty($filename)) {
            $xml->save($filename);
        } else {
            return $xml->saveXML();
        }
    }


}
