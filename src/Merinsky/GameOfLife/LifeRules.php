<?php

declare(strict_types = 1);

namespace Merinsky\GameOfLife;

/**
 * LifeRules
 *
 * @author Miroslav Merinsky <miroslav@merinsky.biz>
 * @version 1.0
 * @licence GPL version 3 http://www.gnu.org/licenses/gpl-3.0.html
 */
class LifeRules
{
    // limit when species die due to isolation
    const RULE_DIE_LIMIT = 1;

    // limit when species die due to overcrowding
    const RULE_DIE_OVERCROWDING_LIMIT = 4;

    // limit when species can create new life
    const RULE_BIRTH_LIMIT = 3;


    /**
     * If there are less than two organisms of one type surrounding
     * one of the same type then it will die due to isolation.
     *
     * @param Life $life
     */
    public static function applyDie(\Merinsky\GameOfLife\Life $life)
    {
        if (!$life->isInitialized())
            throw new \InvalidArgumentException('The life is not initialized.');

        $oldWorld = $life->getWorld();
        $newWorld = $oldWorld;

        for ($x = 1; $x <= $life->getDimension(); $x++) {
            for ($y = 1; $y <= $life->getDimension(); $y++) {
                if ($oldWorld[$x][$y] != Life::EMPTY_CELL) {
                    if (self::countNeighbours($oldWorld, $x, $y, $oldWorld[$x][$y]) <= self::RULE_DIE_LIMIT)
                        $newWorld[$x][$y] = Life::EMPTY_CELL;
                }
            }
        }

        $life->setWorld($newWorld);
    }

    /**
     * If there are four or more organisms of one type surrounding
     * one of the same type then it will die due to overcrowding.
     *
     * @param Life $life
     */
    public static function applyOvercrowding(\Merinsky\GameOfLife\Life $life)
    {
        if (!$life->isInitialized())
            throw new \InvalidArgumentException('The life is not initialized.');

        $oldWorld = $life->getWorld();
        $newWorld = $oldWorld;

        for ($x = 1; $x <= $life->getDimension(); $x++) {
            for ($y = 1; $y <= $life->getDimension(); $y++) {
                if ($oldWorld[$x][$y] != Life::EMPTY_CELL) {
                    if (self::countNeighbours($oldWorld, $x, $y, $oldWorld[$x][$y]) >= self::RULE_DIE_OVERCROWDING_LIMIT)
                        $newWorld[$x][$y] = Life::EMPTY_CELL;
                }
            }
        }

        $life->setWorld($newWorld);
    }

    /**
     * If there are exactly three organisms of one type surrounding one element, they may give birth into that cell.
     * The new organism is the same type as its parents.
     * If this condition is true for more then one species on the same element
     * then species type for the new element is chosen randomly.
     *
     * @param Life $life
     */
    public static function applyBirth(\Merinsky\GameOfLife\Life $life)
    {
        if (!$life->isInitialized())
            throw new \InvalidArgumentException('The life is not initialized.');

        $oldWorld = $life->getWorld();
        $newWorld = $oldWorld;

        for ($x = 1; $x <= $life->getDimension(); $x++) {
            for ($y = 1; $y <= $life->getDimension(); $y++) {
                $neighbour = self::countNeighbours($oldWorld, $x, $y);

                foreach ($neighbour as $species => $number) {
                    if ($number = self::RULE_BIRTH_LIMIT) {
                        // cell is occupied, randomly choose who will survive
                        if ($newWorld[$x][$y] != Life::EMPTY_CELL) {
                            $newWorld[$x][$y] = (rand(0, 1)) ? $newWorld[$x][$y] : $species;
                        } else {
                            $newWorld[$x][$y] = $species;
                        }
                    }
                }
            }
        }

        $life->setWorld($newWorld);
    }

    /**
     * Counts neighbours, if $species is not given, returns associative array of all species (species => $number)
     *
     * @param array $world
     * @param int $x
     * @param int $y
     * @param string $species
     * @return int|array
     */
    private static function countNeighbours(array $world, int $x, int $y, string $species = null)
    {
        if (empty($world) || empty($x) || empty($y))
            throw new \InvalidArgumentException('Invalid argument has been entered.');

        if (isset($species)) {
            $neighbours = 0;

            if (isset($world[$x - 1][$y - 1]) && ($world[$x - 1][$y - 1] == $species))
                $neighbours++;
            if (isset($world[$x][$y - 1]) && ($world[$x][$y - 1] == $species))
                $neighbours++;
            if (isset($world[$x + 1][$y - 1]) && ($world[$x + 1][$y - 1] == $species))
                $neighbours++;
            if (isset($world[$x - 1][$y]) && ($world[$x - 1][$y] == $species))
                $neighbours++;
            if (isset($world[$x + 1][$y]) && ($world[$x + 1][$y] == $species))
                $neighbours++;
            if (isset($world[$x - 1][$y + 1]) && ($world[$x - 1][$y + 1] == $species))
                $neighbours++;
            if (isset($world[$x][$y + 1]) && ($world[$x][$y + 1] == $species))
                $neighbours++;
            if (isset($world[$x + 1][$y + 1]) && ($world[$x + 1][$y + 1] == $species))
                $neighbours++;
        } else {
            $neighbours = [];

            if (isset($world[$x - 1][$y - 1]) && ($world[$x - 1][$y - 1] != Life::EMPTY_CELL)) {
                $species = $world[$x - 1][$y - 1];
                $neighbours[$species] = (isset($neighbours[$species])) ? $neighbours[$species] + 1 : 1;
            }
            if (isset($world[$x][$y - 1]) && ($world[$x][$y - 1] != Life::EMPTY_CELL)) {
                $species = $world[$x][$y - 1];
                $neighbours[$species] = (isset($neighbours[$species])) ? $neighbours[$species] + 1 : 1;
            }
            if (isset($world[$x + 1][$y - 1]) && ($world[$x + 1][$y - 1] != Life::EMPTY_CELL)) {
                $species = $world[$x + 1][$y - 1];
                $neighbours[$species] = (isset($neighbours[$species])) ? $neighbours[$species] + 1 : 1;
            }
            if (isset($world[$x - 1][$y]) && ($world[$x - 1][$y] != Life::EMPTY_CELL)) {
                $species = $world[$x - 1][$y];
                $neighbours[$species] = (isset($neighbours[$species])) ? $neighbours[$species] + 1 : 1;
            }
            if (isset($world[$x + 1][$y]) && ($world[$x + 1][$y] != Life::EMPTY_CELL)) {
                $species = $world[$x + 1][$y];
                $neighbours[$species] = (isset($neighbours[$species])) ? $neighbours[$species] + 1 : 1;
            }
            if (isset($world[$x - 1][$y + 1]) && ($world[$x - 1][$y + 1] != Life::EMPTY_CELL)) {
                $species = $world[$x - 1][$y + 1];
                $neighbours[$species] = (isset($neighbours[$species])) ? $neighbours[$species] + 1 : 1;
            }
            if (isset($world[$x][$y + 1]) && ($world[$x][$y + 1] != Life::EMPTY_CELL)) {
                $species = $world[$x][$y + 1];
                $neighbours[$species] = (isset($neighbours[$species])) ? $neighbours[$species] + 1 : 1;
            }
            if (isset($world[$x + 1][$y + 1]) && ($world[$x + 1][$y + 1] != Life::EMPTY_CELL)) {
                $species = $world[$x + 1][$y + 1];
                $neighbours[$species] = (isset($neighbours[$species])) ? $neighbours[$species] + 1 : 1;
            }
        }

        return $neighbours;
    }

}
