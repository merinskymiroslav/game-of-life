<?php


class LifeTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testLifeExtended()
    {
        $life = new \Merinsky\GameOfLife\LifeExtended();
        $life->fromXML(__DIR__ .'/../../life4x4.xml');

        for ($i = 1; $i <= $life->getInteractions(); $i++) {
            \Merinsky\GameOfLife\LifeRules::applyDie($life);
            \Merinsky\GameOfLife\LifeRules::applyOvercrowding($life);
            \Merinsky\GameOfLife\LifeRules::applyBirth($life);
        }
        $life->setInteractions(0);

        $result = $life->toXML();

        $this->assertEquals($result, file_get_contents(__DIR__.'/../../life4x4_result.xml'));
    }
}