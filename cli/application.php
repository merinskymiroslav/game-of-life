#!/usr/bin/env php
<?php

require __DIR__.'/../vendor/autoload.php';

$application = new \Symfony\Component\Console\Application();
$application->add(new \Merinsky\GameOfLife\Command\LifeCommand());
$application->run();